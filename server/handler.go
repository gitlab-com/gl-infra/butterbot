package server

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"net/url"
	"os/exec"

	"gitlab.com/gl-infra/butterbot/actions"

	"github.com/nlopes/slack"
)

// InteractionHandler handles interactive message response.
type InteractionHandler struct {
	config    InteractionConfig
	messenger Messenger
}

// Messenger is an interface that provides a single method to send a message to a channel
type Messenger interface {
	SendMessage(channel, message string) error
}

// InteractionConfig is an interface that provides a way of reaching the needed values for interactions
type InteractionConfig interface {
	IsValidVerificationToken(string) bool
	GetCommand() string
	GetOption(string) string
}

// New creates a new Interaction handler
func New(c InteractionConfig, m Messenger) InteractionHandler {
	return InteractionHandler{
		config:    c,
		messenger: m,
	}
}

func (h InteractionHandler) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	if r.Method != http.MethodPost {
		log.Printf("[ERROR] Invalid method: %s", r.Method)
		w.WriteHeader(http.StatusMethodNotAllowed)
		return
	}

	buf, err := ioutil.ReadAll(r.Body)
	if err != nil {
		log.Printf("[ERROR] Failed to read request body: %s", err)
		w.WriteHeader(http.StatusInternalServerError)
		return
	}

	jsonStr, err := url.QueryUnescape(string(buf)[8:])
	if err != nil {
		log.Printf("[ERROR] Failed to unespace request body: %s", err)
		w.WriteHeader(http.StatusInternalServerError)
		return
	}

	var message slack.AttachmentActionCallback
	if err := json.Unmarshal([]byte(jsonStr), &message); err != nil {
		log.Printf("[ERROR] Failed to decode json message from slack: %s", jsonStr)
		w.WriteHeader(http.StatusInternalServerError)
		return
	}

	// Only accept message from slack with valid token
	if !h.config.IsValidVerificationToken(message.Token) {
		log.Printf("[ERROR] Invalid token: %s", message.Token)
		w.WriteHeader(http.StatusUnauthorized)
		return
	}

	// Only accept message from slack from an authorized user
	if !actions.IsAllowedFor(message.User.Name) {
		log.Printf("[ERROR] User %s is not in the allowed list", message.User.Name)
		respond(w, message.OriginalMessage,
			fmt.Sprintf("%s is not allowed to reply", message.User.Name), "")
		return
	}

	action := message.Actions[0]
	switch action.Name {
	case actions.ActionSelect:
		value := action.SelectedOptions[0].Value

		// Overwrite original drop down message.
		originalMessage := message.OriginalMessage
		originalMessage.Attachments[0].Text = fmt.Sprintf(
			"Is my purpose to restart gitaly on %s?", value)
		originalMessage.Attachments[0].Actions = []slack.AttachmentAction{
			{
				Name:  "start",
				Text:  "Yes",
				Type:  "button",
				Value: value,
				Style: "primary",
			},
			{
				Name:  "cancel",
				Text:  "No",
				Type:  "button",
				Style: "danger",
			},
		}

		w.Header().Add("Content-type", "application/json")
		w.WriteHeader(http.StatusOK)
		json.NewEncoder(w).Encode(&originalMessage)
		return

	case actions.ActionStart:
		value := h.config.GetOption(action.Value)

		log.Printf("[INFO] starting command execution on %s for %s\n", value, message.User.Name)
		go h.executeCmd(value, message.Channel.Name, message.User.ID)

		respond(w, message.OriginalMessage, "Oh my god...", "")
		return

	case actions.ActionCancel:
		respond(w, message.OriginalMessage, "Action canceled", "")
		return

	default:
		log.Printf("[ERROR] Invalid action was submitted: %s", action.Name)
		w.WriteHeader(http.StatusInternalServerError)
		return
	}
}

func (h InteractionHandler) executeCmd(value, channel, userID string) {
	cmd := exec.Command(h.config.GetCommand(), value)
	out, err := cmd.CombinedOutput()
	if err != nil {
		h.messenger.SendMessage(channel, fmt.Sprintf("<@%s> I failed to fulfil my purpose because\n\n```%s\n%s```",
			userID, out, err))
		log.Printf("[ERROR] command failed: %s %s", err, out)
		return
	}
	log.Printf("[INFO] command on %s succeeded: %s", value, out)
	h.messenger.SendMessage(channel, fmt.Sprintf("<@%s> I fulfilled my purpose:\n\n```%s```", userID, out))
}

func respond(w http.ResponseWriter, original slack.Message, title, value string) {
	original.Attachments[0].Actions = []slack.AttachmentAction{} // empty buttons
	original.Attachments[0].Fields = []slack.AttachmentField{
		{
			Title: title,
			Value: value,
			Short: false,
		},
	}

	w.Header().Add("Content-type", "application/json")
	w.WriteHeader(http.StatusOK)
	json.NewEncoder(w).Encode(&original)
}
