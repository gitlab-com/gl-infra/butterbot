FROM alpine:3.6
 
RUN apk add --no-cache openssh-client

COPY butterbot /
COPY butterbot-launcher /
 
ENTRYPOINT /butterbot-launcher
