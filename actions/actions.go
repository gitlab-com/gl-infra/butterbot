package actions

const (
	// ActionSelect is the selection phase
	ActionSelect = "select"
	// ActionStart is used to start the execution
	ActionStart = "start"
	// ActionCancel is used to cancel the process
	ActionCancel = "cancel"
)

var users Allowed

type Allowed struct {
	allowedUsers map[string]bool
}

func IsAllowedFor(user string) bool {
	_, ok := users.allowedUsers[user]
	return ok
}

func Initialize(allowedUsers map[string]bool) {
	users = Allowed{
		allowedUsers: allowedUsers,
	}
}
