package main

import (
	"log"
	"net/http"
	"os"
	"strings"

	"github.com/kelseyhightower/envconfig"
	"gitlab.com/gl-infra/butterbot/actions"
	"gitlab.com/gl-infra/butterbot/server"
	"gitlab.com/gl-infra/butterbot/slack"
)

// https://api.slack.com/slack-apps
// https://api.slack.com/internal-integrations
type envConfig struct {
	// Port is server port to be listened.
	Port string `envconfig:"PORT" default:"3000"`

	// BotToken is bot user token to access to slack API.
	BotToken string `envconfig:"BOT_TOKEN" required:"true"`

	// VerificationToken is used to validate interactive messages from slack.
	VerificationToken string `envconfig:"VERIFICATION_TOKEN" required:"true"`

	// BotID is bot user ID.
	BotID string `envconfig:"BOT_ID" required:"true"`

	// ChannelID is slack channel ID where bot is working.
	// Bot responses to the mention in this channel.
	ChannelsID []string `envconfig:"CHANNELS_ID" required:"true"`

	// Allowed slack users that can authorize this command, separated by comma
	AllowedUsers []string `envconfig:"ALLOWED_USERS" required:"true"`

	// Options that are going to be presented in the following format:
	// key=value,key=value
	Options map[string]string `envconfig:"OPTIONS" required:"true"`

	// Cmd the command that will be executed based in the Option value, like
	// exec(Sprintf("%s %s", Cmd, value))
	Cmd string `envconfig:"CMD" required:"true"`

	// Debug enables debug mode for the slack client
	Debug bool `envconfig:"DEBUG" required:"false" default:"false"`

	// Valid channel
	validChannels map[string]bool
}

func main() {
	os.Exit(_main())
}

func _main() int {
	var env envConfig
	if err := envconfig.Process("", &env); err != nil {
		log.Printf("[ERROR] Failed to process env var: %s", err)
		return 1
	}

	actions.Initialize(env.GetUsers())

	// Listening slack event and response
	slackListener := slack.New(env.BotToken, env)
	go slackListener.Listen()
	log.Printf("[INFO] Start slack event listening")

	// Register handler to receive interactive message
	// responses from slack (kicked by user action)
	http.Handle("/interaction", server.New(env, slackListener))

	log.Printf("[INFO] Server listening on :%s", env.Port)
	if err := http.ListenAndServe(":"+env.Port, nil); err != nil {
		log.Printf("[ERROR] %s", err)
		return 1
	}

	return 0
}

func (e envConfig) GetUsers() map[string]bool {
	users := make(map[string]bool, 0)
	for _, user := range e.AllowedUsers {
		users[strings.TrimSpace(user)] = true
	}
	return users
}

func (e envConfig) isAllowedUser(user string) bool {
	users := e.GetUsers()
	_, ok := users[user]
	return ok
}

func (e envConfig) GetOptions() map[string]string {
	return e.Options
}

func (e envConfig) GetOption(opt string) string {
	return e.Options[opt]
}

func (e envConfig) IsValidVerificationToken(token string) bool {
	return token == e.VerificationToken
}

func (e envConfig) GetCommand() string {
	return e.Cmd
}

func (e envConfig) GetBotID() string {
	return e.BotID
}

func (e envConfig) IsValidChannel(channel string) bool {
	if e.validChannels == nil { // This way it is an amortized algorithm
		validChannels := make(map[string]bool)
		for _, c := range e.ChannelsID {
			validChannels[c] = true
		}
		e.validChannels = validChannels
	}
	_, ok := e.validChannels[channel]
	return ok
}

func (e envConfig) GetDebug() bool {
	return e.Debug
}
